\documentclass[12pt]{article}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{commath}
\usepackage{textcomp}
\usepackage{siunitx}
\usepackage{graphicx}
\title{The Mathematics of Robotic Movement}
\date{April 26, 2016}
\author{John Wesley McDonald Hayhurst}

\begin{document}
  \maketitle
  \newpage

  \section{Dick}
  Most people, when they think of Robots, do not really think of complex mathematical equations. Most people think that most robotics work is done with simple code that tells the robot to move its arm in a certain direction. This however is not the case, most robotic movement is actually matrix transformations and quaternions. While most of these transformations are done in code, you will still need to understand how transformations work and the values needed for those matrices. Without this information getting the robot to interact with anything other than itself is impossible. To gain a better understanding of the mathematics of robotic movement we will be looking at the basic concepts of rotation frames, matrix transformations, quaternions, and some advanced techniques.

  \section{Rigid Body Transformations}
  To figure out how to move a robot, we must first figure out how to represent the robotic with respect to the world. The \textit{workspace} is the area in which the robot will be acting upon and in. The \textit{configuration} space is then the collection of all of the points on the object relative to the workspace. A reference frame is an axis for defining movement in a configuration space. For robotic movement forward movement is represented by the $x$-axis and a sliding motion is the $y$-axis. With the configuration space defined we then can start moving things across said space. Starting simply we take a rigid body or frame and then move it to a specific point into the space. With the location of these points, starting and ending, we can use a transformation matrix to move the robot to said point. These transformations are of the form $^{W}T_{P}$, where $W$ is the world matrix and $P$ is the point in the world frame that you are trying to move to. To move in the $x$ or $y$ direction requires building a transformation matrix similar to the one shown below, where $x$ and $y$ are the distances you are moving in those planes respectively.
  \begin{equation}
    ^{W}T_{P} = \begin{bmatrix} 1 & 0 & x \\ 0 & 1 & y \\ 0 & 0 & 1 \end{bmatrix}
  \end{equation}
  We now know how to move our rigid body across the space. To rotate our rigid bodies we use a rotation matrix. This rotation matrix depends on the axis on which we rotate the object frame. The following matrixes are the corresponding rotation matrices for $z$, $x$, and $y$.
  \begin{equation}
    \begin{split}
      R_{z}(\theta) &= \begin{bmatrix} \cos{\theta} & -\sin{\theta} & 0 \\ \sin{\theta} & \cos{\theta} & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
      R_{x}(\theta) &= \begin{bmatrix} 1 & 0 & 0 \\ 0 & \cos{\theta} & -\sin{\theta} \\ 0 & \sin{\theta} & \cos{\theta} \end{bmatrix}\\
      R_{y}(\theta) &= \begin{bmatrix} \cos{\theta} & 0 & \sin{\theta} \\ 0 & 1 & 0 \\ -\sin{\theta} & 0 & \cos{\theta} \end{bmatrix}
    \end{split}
  \end{equation}
  In general, when rotating rigid bodies, we rotate along the $z$-axis. Now that we know how to rotate our rigid body we can then proceed in showing how to move the rigid body across the work space. When doing these matrix transformations we must be careful to remember that these transformations are not commutative. This means that when you are multiplying matrices order matters, matrix $A \cdot B$ is not the same as $B \cdot A$. Considering the following problem:

  A robot is docked at its base station, where we assign the world frame. Assume the robot body frame is aligned with the world frame when docked. The robot leaves the base station and first moves 2 meters forward (body $x$-axis) to (pose $A$), rotates 45 degrees counter clockwise (pose $B$), moves 0.5 meters forward (Pose $C$), then rotates 30 degrees clock wise (pose $D$), and moves another 0.25 meters forward to the final (pose $E$).\cite{IR}
  A pose in this case is the position and rotation the robot has taken relative to the previous position.

  To find the final configuration of the robot we need to do a series of transformations between each frame from $A$-$B$-$C$-$D$-$E$. So then our transformation equation should be
  \begin{equation}
    ^{W}T_{E} = ^{W}T_{A} \cdot ^{A}T_{B} \cdot ^{B}T_{C} \cdot ^{C}T_{D} \cdot ^{D}T_{E}
  \end{equation}
  Looking at the equation piecewise $^{W}T_{E}$ is the final position of the robot relative to the world. $^{W}T_{A}$ is the robots position relative to the world frame. $^{A}T_{B}$ is the position of the robot relative to the starting position. The remaining transformations then translate the movements relative to the previous one.

  The first part is creating pose $A$'s matrix. Pose $A$ has us driving 2 meters forward meaning that we are traveling along the $x$-axis. So our transformation matrix is
  \begin{equation}
    ^{W}T_{A} = \begin{bmatrix} 1 & 0 & 2 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}.
  \end{equation}
  Now that we now how to construct forward movement matrices let us then look at rotational. Luck for us this problem has us moving forward and then turning. We then turn 45 degrees counter clockwise after moving to pose $A$. So our transformation matrix will be
  \begin{equation}
    ^{A}T_{B} = \begin{bmatrix} \cos{\frac{\pi}{4}} & -\sin{\frac{\pi}{4}} & 0 \\ \sin{\frac{\pi}{4}} & \cos{\frac{\pi}{4}} & 0 \\ 0 & 0 & 1 \end{bmatrix}.
  \end{equation}
  We used a rotation matrix along the $z$-axis. We do this because we cannot turn the robot among the $y$ and $x$ axis. Consider the reference frame we use for the robot
  \begin{center}
    \includegraphics[scale = .8, center]{reference_frame.png}
  \end{center}

  The Robot is rotating so it can make turns without sliding, the logical choice for rotating would be the $z$-axis. Next we chose $\frac{\pi}{4}$ because we are turning 45 degrees counter clockwise. The remaining transformations should be easy to get following the same rule of thumb giving us the following matrix transformation equation
  \begin{equation}
    \begin{split}
      ^{W}T_{E} &= ^{W}T_{A} \cdot ^{A}T_{B} \cdot ^{B}T_{C} \cdot ^{C}T_{D} \cdot ^{D}T_{E}\\
      ^{W}T_{E} &= \begin{bmatrix} 1 & 0 & 2 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}
                   \begin{bmatrix} \cos{\frac{\pi}{4}} & -\sin{\frac{\pi}{4}} & 0 \\ \sin{\frac{\pi}{4}} & \cos{\frac{\pi}{4}} & 0 \\ 0 & 0 & 1 \end{bmatrix}
                   \begin{bmatrix} 1 & 0 & 0.5 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}
                   \begin{bmatrix} \cos{\frac{-\pi}{6}} & -\sin{\frac{-\pi}{6}} & 0 \\ \sin{\frac{-\pi}{6}} & \cos{\frac{-\pi}{6}} & 0 \\ 0 & 0 & 1 \end{bmatrix}
                   \begin{bmatrix} 1 & 0 & 0.25 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
      ^{W}T_{E} &= \begin{bmatrix} 0.965926 & -0.258819 & 2.59503 \\ -0.258819 & 0.965926 & 0.418258 \\ 0 & 0 & 1 \end{bmatrix}
    \end{split}
  \end{equation}
  The matrix above is pose $E$ relative to the world frame. This matrix tells us the the $x$ and $y$ coordinates which are, 2.59503 and 0.418258 respectively. We can also find the angle the robot is facing in pose $E$ by taking the inverse tangent of the two angles. Doing so will result in the rotational angle at pose $E$, relative to the world frame, being 15 degrees counter clockwise.

  Now consider since we can find poses relative to the world frame then we are able to find an object relative to the world frame. To do this we need the position of the object relative to the robot. From there we can do matrix transformations to then get the point relative to the world frame. But before we are able to do that we need to look at how robots detect objects.

  Robots can detect objects in multiple ways but the most common and the most useful one is LIDAR. \textit{LIDAR} stands for Light Detection And Ranging. This is a tool that uses lasers to detect objects and how far away they are. Most robots have a LIDAR detection system that will constantly send out laser beams to actively detect objects in the configuration space. The LIDAR will sweep out a beam being reflected by a mirror. If the laser beam is reflected back by an object it will then be considered as an object. Since the LIDAR detects things in a sweeping motion relative to where it is mounted, its position will be detected at an radius and an angle from the LIDAR. In general the LIDAR matrix will not be a homogenous transformation matrix but will rather be a three by one matrix. In general LIDAR detection will be of the form
  \begin{equation}
    Q = \begin{bmatrix} r * \cos{\theta} \\ r * \sin{\theta} \\ 1 \end{bmatrix}
  \end{equation}
  Where $r$ is the radius and $\theta$ is the angle at which the LIDAR detected the object.

  To see how this is used for detecting the position of an object relative to the world frame consider the following problem taken from the \textit{Introduction to Robotics} course at Christopher Newport University.
  A robot moves across the floor and arrives at ($x,y,\theta$) $=$ ($3.5,-0.25, \frac{\pi}{6}$) in world frame. It has a LIDAR mounted at ($0.1,0,0$) relative to the robot body frame. The LIDAR senses an object at $r = 0.5$ meters and $\theta = \frac{\pi}{10}$. Where is this object in the world frame?

  The robot is located somewhere in the world frame and the LIDAR is mounted onto the robot. The LIDAR detects an object at a position relative to the LIDAR. Using what we know about LIDAR this problem should be similar to the first example problem. The first thing we do is set up our transformation equation
  \begin{equation}
    ^{W}T_{O} = ^{W}T_{R} \cdot ^{R}T_{L} \cdot Q_{O}
  \end{equation}
  Where $W$ is the world frame, $R$ is the robot frame, $L$ is the LIDAR frame, and $Q_{O}$ is the detected object relative to the LIDAR frame. Using this information we can set up the translation matrix for the world frame to the robot and to the robot to the LIDAR using what we already know about transformation matrices.
  \begin{equation}
    \begin{split}
      ^{W}T_{R} &= \begin{bmatrix} \cos{\frac{\pi}{6}} & -\sin{\frac{\pi}{6}} & 3.5 \\ \sin{\frac{\pi}{6}} & \cos{\frac{\pi}{6}} & -0.25 \\ 0 & 0 & 1 \end{bmatrix}\\
      ^{R}T_{L} &= \begin{bmatrix} 1 & 0 & 0.1 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
    \end{split}
  \end{equation}
  So now we move on to the object relative to the LIDAR. Using the information above now that the LIDAR detected an object that is $0.5$ away at an angle of $\pi/10$ relative to the LIDAR frame. This allows us to set up the non homogenous transformation matrix.
  \begin{equation}
    Q_{O} = \begin{bmatrix} 0.5 * \cos{\frac{\pi}{10}} \\ 0.5 * \sin{\frac{\pi}{10}} \\ 1 \end{bmatrix}
  \end{equation}
  With $Q_{O}$ being defined we can then set up our full transformation equation to find the object relative to the world frame.
  \begin{equation}
    \begin{split}
      ^{W}T_{O} &= ^{W}T_{R} \cdot ^{R}T_{L} \cdot Q_{O}\\
      ^{W}T_{O} &= \begin{bmatrix} \cos{\frac{\pi}{6}} & -\sin{\frac{\pi}{6}} & 3.5 \\ \sin{\frac{\pi}{6}} & \cos{\frac{\pi}{6}} & -0.25 \\ 0 & 0 & 1 \end{bmatrix}  \begin{bmatrix} 1 & 0 & 0.1 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix} \begin{bmatrix} (0.5 * \cos{\frac{\pi}{10}}) \\ (0.5 * \sin{\frac{\pi}{10}}) \\ 1 \end{bmatrix} \\
      ^{W}T_{O} &= \begin{bmatrix} 3.9212 \\ 0.1716 \\ 1 \end{bmatrix}
    \end{split}
  \end{equation}
  So the object is located relative to the world frame at coordinates $x = 3.9212$ and $y = 0.1716$. The question of what rotation is the object facing cannot be answered. Since the LIDAR only gives us information based on an object detected that information is largely irrelevant. If you wanted to know the objects rotation, you would have to know its rotation relative to the LIDAR. The LIDAR is not capable of detecting the rotation of an object only that the object reflected back a laser beam. We can however use the first example of rigid body movement but replace the last pose with an object relative to the robot.

  \section{Quaternion Representations}

  We have previously only looked at 2 dimensional movement for our rigid bodies. The reasoning behind this is that the rigid body cannot just move along the $z$-axis. Robots are more than just rigid bodies and have 6 degrees of freedom. \textit{Degrees of freedom} describe the movement a body can make without being hindered. For 3 dimensional bodies we have 6 degrees of freedom, elevating, strafing, surging, roll, pitch, and yaw. Elevating describes movement in the $z$-axis, strafing is movement in the $y$-axis, surging is movement in the $x$-axis, roll is the rotation in the $x$-axis, pitch is the rotation in the $y$-axis, and yaw is the rotation in the $z$-axis. As a result of this a standard 3 dimensional transformation matrix is similar to that of a 2 dimensional matrix and takes on the standard form
  \begin{equation}
    \begin{bmatrix} \hat{R} & \hat{t} \\ \hat{0} & 1 \end{bmatrix},
  \end{equation}
  where $\hat{R}$ is a 3 x 3 rotational matrix, $\hat{t}$ is a 3 x 1 translation matrix, and $\hat{0}$ is a 1 x 3 matrix of all zeroes.

  Consider the example of an arm rotating about the $y$-axis of radius $\frac{\pi}{4}$ and is located at position $x = 3.5$, $y = 3.0$, and $z = 2.0$. We can then easily set up this translation matrix. First consider that the robot is rotating its arm among the $y$-axis at $\theta = \frac{\pi}{4}$. We can then set up the rotational matrix needed for the 3 dimensional transformation matrix.
  \begin{equation}
    \hat{R} = \begin{bmatrix} \cos{\frac{\pi}{4}} & 0 & \sin{\frac{\pi}{4}} \\ 0 & 1 & 0 \\ -\sin{\frac{\pi}{4}} & 0 & \cos{\frac{\pi}{4}}   \end{bmatrix}
  \end{equation}
  Then we can set up the translation matrix which takes on the general form
  \begin{equation}
      \hat{t} = \begin{bmatrix} x \\ y \\ z \end{bmatrix}
  \end{equation}
  With that knowledge we then can proceed to set up our 3D transformation matrix for this robotic arm movement. Putting all the pieces together we get
  \begin{equation}
    \begin{bmatrix} \cos{\frac{\pi}{4}} & 0 & \sin{\frac{\pi}{4}} & 3.5 \\ 0 & 1 & 0 & 3.0 \\ -\sin{\frac{\pi}{4}} & 0 & \cos{\frac{\pi}{4}} & 2.0 \\ 0 & 0 & 0 & 1 \end{bmatrix}
  \end{equation}

  With the knowledge of 3 dimensional transformation matrices the need for quaternions arise. Quaternions are used to represent orientation and rotation of objects in three dimensional space. Since all robotic movement takes place in a three dimensional space they are an easier way of representing these movements. More importantly quaternions avoid gimble lock. \textit{Gimble lock} is when one degree of freedom is lost due to when 2 axises are driven into a parallel configuration, thus locking the system into rotation in a two dimensional space. While gimble lock does not occur that often in your standard robot usage, it is important to protect against potential problems.

  Quaternions are represented by the standard equation of
  \begin{equation}
    \begin{split}
      q &= [q_{w}(q_{x}, q_{y}, q_{z})] \\
      q &= q_{w} + q_{x}i + q_{y}j + q_{z}k
    \end{split}
  \end{equation}
  Where $q_{w}$ is a scalar and $q_{x}i + q_{y}j + q_{z}k$ is a vector.

  Using this basis we can convert a quaternion into a standard transformation matrix. Consider the rotation matrix
  \begin{equation}
    \begin{bmatrix} m_{00} & m_{01} & m_{02} \\ m_{10} & m_{11} & m_{12} \\ m_{20} & m_{21} & m_{22} \end{bmatrix}
  \end{equation}
  This matrix can then be converted into a quaternion via the following equations
  \begin{equation}
    \begin{split}
      q_{w} &= \sqrt{1 + m_{00} + m_{11} + m_{22}}/2 ,\\
      q_{x} &= (m_{21} - m_{12}) / (4q_{w}), \\
      q_{y} &= (m_{02} - m_{20}) / (4q_{w}), and\\
      q_{z} &= (m_{10} - m_{01}) / (4q_{w}).
    \end{split}
  \end{equation}
  Given a standard quaternion you can convert it into a rotational matrix. The conversion will take on the form
  \begin{equation}
    \begin{bmatrix} 1 - 2q_{y}^2 - 2q_{z}^2 & 2q_{x}q_{y} - 2q_{z}q_{w} & 2q_{x}q_{z} + 2q_{y}q_{w} \\ 2q_{x}q_{y} + 2q_{z}q_{w} & 1 - 2q_{x}^2 - 2q_{z}^2 & 2q_{y}q_{z} - 2q_{x}q_{w} \\ 2q_{x}q_{z} - 2q_{y}q{w} & 2q_{y}q_{z} + 2q_{x}q_{w} & 1 - 2q_{x}^2 - 2q_{y}^2  \end{bmatrix}.
  \end{equation}
  While quaternions are extremely important for three dimensional robotic movement, their application is generally handled by computer programs. Most programs when giving commands to a robot will generally convert a 3 dimensional rotation matrix into a quaternion for you. None the less it is important to know how these conversions are happening and why they are happening.

  \section{Kinematics}
  \textit{Kinematics} is known as the geometry of motion ignoring dynamics. For a simple model this is a good representation of robotic movement. Using kinematics for robotic movement provides good baseline for how a robot should move to a point in the world. There are a couple of different models for robotic movement that are useful to know.

  The first model is the \textit{differential drive} or diff-drive model. This is used when a robot has two wheels equally spaced apart where the wheels do not rotate. To obtain a rotation the wheels will spin at different rates thus achieving a rotation. For example, a robot wanting to make a right turn will turn its right wheel at a slower speed than its left wheel. We then can take the wheel velocity as inputs into our system. Let us then denote the wheel velocities as $\psi_{\ell}$ and $\psi_{r}$ for the left and right wheel respectively. Additionally, $\rho$ is the radius of the circle on which the robot is turning.
  Using differential geometry we can obtain the matrix of the velocity with respect to the $x$ axis and rotation angle of the body frame of the robot. We then get the equation of
  \begin{equation}
    \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} = \begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix} \begin{bmatrix} \psi_{\ell} \\ \psi_{r} \end{bmatrix}
  \end{equation}\label{DIFF}
  with $c$ being the distance between the wheel and the center of the axle and $R$ being the wheel radius.
  We can use similar angles and geometry to get the relation of $v_{x}$, $\omega_{z}$, and $\rho$
  \begin{equation}
    v_{x} = \rho * \omega_{z}
  \end{equation}
  The diff-drive model is pictured bellow
  \begin{center}
    \includegraphics[scale = .8, center]{diffdrive.png}
  \end{center}

  The next model to look at is the \textit{bicycle model}. This model can also be used as a simplified version of a car. To use this model we must have one wheel fixed and the other wheel freely able to rotate, just like a bicycle. The model has three more variables, $\phi$ which is the steering angle, $L$ which is the distance between the front and the rear wheels, and $\theta$ which is the angle of body rotation.

  The bicycle model can be represented by
  \begin{equation}
    \begin{split}
      \dot{q} &= \begin{bmatrix} \dot{x} \\ \dot{y} \\ \dot{\theta} \\ \dot{\phi} \end{bmatrix} \\
      \dot{q} &= \begin{bmatrix} \cos{\theta} \\ \sin{\theta} \\ \tan{\phi}/L \\ 0 \end{bmatrix}v_{x} + \begin{bmatrix} 0 \\ 0 \\ 0 \\ 1 \end{bmatrix}\omega_{\phi}.
    \end{split}
  \end{equation}

  Using similar angles and geometry we can obtain
  \begin{equation}
    \begin{split}
      \tan{\theta} &= L/\rho \\
      v_{x} &= \rho * \omega_{z}.\\
    \end{split}
  \end{equation}

  The bicycle model is pictured below
  \begin{center}
    \includegraphics[scale = .6, center]{bicycle.png}
  \end{center}

  Consider an example of a diff-drive model taken from the Intro to Robotics course.
  Our iRobot Create, with wheels that are 64.5 millimeters in diameter spaced a distance of 258 mm apart, is traveling at a forward velocity of 0.2 m/s. We need it to turn left and have its center reference point follow a circular arc with radius of 2 meters while maintaining its current speed.
  Neglecting the transient as you accelerate to change wheel speeds.
  What is the body rotation rate required to follow the arc?
  What are the required wheel speeds?\cite{IR}

  First we determine what is given to us. We know that $R = 0.0645/2$ meters since the diameters of the wheels is $64.5$ millimeters. Then we have $c = 0.258/2$ meters since the distance is between the wheel and the axis. We also know that the robot is moving $0.2$ m/s and that the radius of the desired arc, $\rho$, is $2$ meters.

  To determine the body rotation rate ($\omega_{z}$) given the velocity and $\rho$ we substitute them in and solve for the body rate.
  \begin{equation}
    \begin{split}
      v_{x} &= \rho \cdot  \omega_{z}\\
      \omega_{z} &= {v_{x} \over \rho} \\
      \omega_{z} &= {0.2 \over 2}\\
      \omega_{z} &= 0.1.
    \end{split}
  \end{equation}
  Thus the rotation rate of the robot is $0.1$ radians/sec.

  To determine the required wheel speeds we will set up equation (20) to solve for $\psi_{\ell}$ and $\psi_{r}$.
  \begin{equation}
    \begin{split}
      \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} &= \begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix} \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} \\
      \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &=   {\begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix}}^{-1} \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} \\
      \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &=   {\begin{bmatrix} {0.03233 \over 2} & {0.03233 \over 2} \\ {-0.03233 \over {0.258}} & {0.03233 \over {0.258}} \end{bmatrix}}^{-1} \begin{bmatrix} 0.2 \\ 0.1 \end{bmatrix} \\
      \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &= \begin{bmatrix} 5.801 \\ 6.601 \end{bmatrix}\\
    \end{split}
  \end{equation}

  Now that we have looked at a diff drive model. Consider an example of a bicycle model taken from the \textit{Introduction to Robotics} course.
  With a wheelbase (distance between front and rear tires) of 98.3 cm, assume the bike races around a right corner along a circular arc of 10 m at 6.7 m/s. Use the simple model even though racing bikes lean during turns. What steering angle of the front wheel is required? What is the rate of angular rotation of the bike?\cite{IR}

  We know that $L = 0.983$, $\rho = 10$, and $v_{x} = 6.7$. It will be easier if we solve for the rate of angular rotation, as while it is not needed to solve for the steering angle, it makes it easier to solve.
  \begin{equation}
    \begin{split}
      v_{x} &= \rho \omega_{z} \\
      \omega_{z} &= {v_{x} \over \rho} \\
      \omega_{z} &= {6.7 \over 10} \\
      \omega_{z} &= 0.67 \\
    \end{split}
  \end{equation}
  The rate of angular rotation is 0.67 radians/sec. Now moving on to the steering angle
  \begin{equation}
    \begin{split}
      \dot{\theta} &= \omega_{z} = v_{x}{(\tan{\phi})\over L} \\
      \tan{\phi} &= {(\omega_{z} L) \over v_{x}} \\
      \tan{\phi} &= {L \over \rho} \\
      \tan{\phi} &= 0.0983 \\
      \phi &= \tan^{-1}{0.0983} \\
      \phi &= 5.6 \si{\degree} \\
    \end{split}
  \end{equation}
  So the resulting steering angle is $5.6 \si{\degree}$

  \section{Advanced Techniques}
  With the basics of robotic movement down we can use these equations to physically move our robot. The only problem is that life is not kinematics. As a result of this our simulated movements of the robot will be accurate but when we physically put the robot onto the floor, the robot can slip, slide, and can go off track. This method is called \textit{dead reckoning} and it allows for wide margins of error. To combat this we will use a method known as pure pursuit. \textit{Pure pursuit} is a combination of methods and technologies that we have already discussed.

  Pure pursuit uses a combination of LIDAR and Matrix transformations to correct for any offset. The program will place a marker in the configuration space and direct the robot to travel to it while avoiding obstacles. This is made by either knowing the point in the world frame that you want the robot to move to, or using matrix transformations to obtain the point. With that digital point created it uses its LIDAR to avoid obstacles and move to the desired point.

  The creation of waypoints is mostly handled by the software you are using but allows for dynamic movement and for the robot to navigate in a real world environment. Pure pursuit is one option to allow for real world correction while driving a robot. Many other options exist for correction of errors as a result of kinematics. Pure pursuit is the easiest option as other options include the use of AI, lattice path planning, or real time controlling and tuning. Pure pursuit is the only options out of these that allows for full autonomous control of a robot.

  \section{Conclusions}
  Robotic movement is mostly dictated by transformation matrices. These matrices are good for two dimensional movement. When it comes to three dimensional movement quaternions are extremely important as they allow us to avoid gimble lock. For simple models and simulations kinematics is a great way to move the robot using translations matrices. Unfortunately real life is not kinematics and to get accurate results we must use some advanced techniques. The easiest technique to use is pure pursuit which requires a LIDAR to map out obstacles in the world and to create digital waypoints for the robot to travel to. This is the beginning of learning how to move a robot. Using this information moving a robot around should now be simple and easy to set up.

  \bibliographystyle{unsrt}
  \bibliography{MoR}
\end{document}
