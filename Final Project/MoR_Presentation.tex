\documentclass{beamer}
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
 \usetheme{Warsaw}
\usepackage{amsmath, amssymb, mathrsfs, amsopn, amsfonts, amstext,color}
\usepackage{graphicx}
\beamertemplatenavigationsymbolsempty

\definecolor{cnuBlue}{RGB}{0,44,118}
\definecolor{cnuAthletic}{RGB}{0,57,116}
\definecolor{cnuGray}{RGB}{165,172,176}
\definecolor{cnuSilver}{RGB}{132,136,139}
\definecolor{red}{RGB}{255,0,0}

\title[Mathematics of Robotic Movement]{Mathematics of Robotic Movement}
\date{\today}
\subtitle{Mathematical Writing 301}
\author[Hayhurst]{John Wesley McDonald Hayhurst}

\begin{document}
  \section{Mathematics of Robotic Movement}
  \subsection{Introduction to Robotic Movement}
  \begin{frame}\frametitle{Introduction}
  \maketitle
  \end{frame}

  \begin{frame}\frametitle{Reference Frames}
    \begin{center}
      \includegraphics[scale = .8, center]{reference_frame.png}
      \begin{itemize}
        \item World frame
        \item Robot frame
        \item Rotation \& translation
      \end{itemize}
    \end{center}
  \end{frame}

  \begin{frame}\frametitle{Rigid Body Movement \& Translation Matrices}
    \begin{itemize}
      \item To move the rigid body across the world you will use a translation matrix
      \item These translation matrices are made up relative positions and rotations
      \item Depending on which axis you are rotating upon the rotation matrix will differ
      \item These matrices are relative to one another, thus we can translate them back to our world frame
    \end{itemize}
  \end{frame}

  \begin{frame}\frametitle{Simple Matrix Translation}
    \begin{equation}
      ^{W}T_{R} \cdot ^{R}T_{A} = ^{W}T_{A}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Translation Matrix \& Rotation Matrices General Forms}
    \begin{equation}
      ^{W}T_{R} = \begin{bmatrix} 1 & 0 & x \\ 0 & 1 & y \\ 0 & 0 & 1 \end{bmatrix}.
      \begin{split}
        R_{z}(\theta) &= \begin{bmatrix} \cos{\theta} & -\sin{\theta} & 0 \\ \sin{\theta} & \cos{\theta} & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
        R_{x}(\theta) &= \begin{bmatrix} 1 & 0 & 0 \\ 0 & \cos{\theta} & -\sin{\theta} \\ 0 & \sin{\theta} & \cos{\theta} \end{bmatrix}\\
        R_{y}(\theta) &= \begin{bmatrix} \cos{\theta} & 0 & \sin{\theta} \\ 0 & 1 & 0 \\ -\sin{\theta} & 0 & \cos{\theta} \end{bmatrix}
      \end{split}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Simple Translation Example}
    Our Robot starts off at the world frame and then moves forward in the x axis 2 meters. From there our robot rotates counter clockwise 45 degrees.
    \begin{equation}
      \begin{split}
      ^{W}T_{R} &= \begin{bmatrix} 1 & 0 & 2 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
      ^{R}T_{A} &= \begin{bmatrix} \cos{\frac{\pi}{4}} & -\sin{\frac{\pi}{4}} & 0 \\ \sin{\frac{\pi}{4}} & \cos{\frac{\pi}{4}} & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
      \end{split}
      \begin{split}
        ^{W}T_{A} &= ^{W}T_{R} \cdot ^{R}T_{A}\\
        ^{W}T_{A} &= \begin{bmatrix} 1 & 0 & 2 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix} \begin{bmatrix} \cos{\frac{\pi}{4}} & -\sin{\frac{\pi}{4}} & 0 \\ \sin{\frac{\pi}{4}} & \cos{\frac{\pi}{4}} & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
        ^{W}T_{A} &= \begin{bmatrix} \sqrt{2}/2 & -\sqrt{2}/2 & 2 \\ \sqrt{2}/2 & \sqrt{2}/2 & 0 \\ 0 & 0 & 1 \end{bmatrix}\\
      \end{split}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Kinematics}
    \begin{itemize}
      \item \textit{Kinematics} is the geometry of motion ignoring dynamics
      \item LIFE IS NOT KINEMATICS
      \item Kinematics is used for simulation of robotic movements
      \item With some advanced techniques we can use kinematics for real life applications
    \end{itemize}
  \end{frame}

  \begin{frame}\frametitle{Differential Drive Model}
    \begin{itemize}
      \item The \textit{Diff-Drive} model is a two wheel model
      \item These models use two wheels to make the robot move and turn by rotating the two wheels at different speeds
    \end{itemize}
  \end{frame}

  \begin{frame}\frametitle{Differential Drive Model}
    \includegraphics[scale = .3, center]{DifferentialDriveRobot.png}
  \end{frame}

  \begin{frame}\frametitle{Differential Drive Model}
    \begin{equation}
      \begin{split}
      \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} &= \begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix} \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix}\\
      v_{x} &= \rho * \omega_{z}
    \end{split}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Differential Drive Model}
    \begin{itemize}
      \item $c$ is the distance between the wheel and the axel
      \item $R$ is the radius of the wheels
      \item $v_{x}$ is the forward velocity of the robot
      \item $\omega_{z}$ is the body rotation of the robot
      \item $\rho$ is the radius of the arc the robot is following
    \end{itemize}
  \end{frame}

  \begin{frame}\frametitle{Diff-Drive Example}
    Our iRobot Create, with wheels that are 64.5 millimeters in diameter and spaced a distance of 258 mm between each other, is traveling    at a forward velocity of 0.2 m/s. We need it to turn left and have it's center reference point follow a circular arc with radius of 2 meters while maintaining it's current speed. \cite{IR}
  \end{frame}

  \begin{frame}\frametitle{Diff-Drive Example}
    Neglecting the transient as you accelerate to change wheel speeds.
    What is the body rotation rate required to follow the arc?
    What are the required wheel speeds?
  \end{frame}

  \begin{frame}\frametitle{Diff-Drive Example}
    \begin{equation}
      \begin{split}
        v_{x} &= \rho * \omega_{z}\\
        \omega_{z} &= {v_{x} \over \rho} \\
        \omega_{z} &= {0.2 \over 2}\\
        \omega_{z} &= 0.1
      \end{split}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Diff-Drive Example}
    \begin{equation}
      \begin{split}
        \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} &= \begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix} \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} \\
        \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &=   {\begin{bmatrix} {R \over 2} & {R \over 2} \\ {-R \over {2c}} & {R \over {2c}} \end{bmatrix}}^{-1} \begin{bmatrix} v_{x} \\ \omega_{z} \end{bmatrix} \\
        \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &=   {\begin{bmatrix} {0.03 \over 2} & {0.03 \over 2} \\ {-0.03 \over {0.258}} & {0.03 \over {0.258}} \end{bmatrix}}^{-1} \begin{bmatrix} 0.2 \\ 0.1 \end{bmatrix} \\
        \begin{bmatrix} \psi_{l} \\ \psi_{r} \end{bmatrix} &= \begin{bmatrix} 5.801 \\ 6.601 \end{bmatrix}\\
      \end{split}
    \end{equation}
  \end{frame}

  \begin{frame}\frametitle{Overview}
    \begin{itemize}
      \item Reference frames are important \& necessary
      \item Translation matrices
      \item Life is not kinematics, but kinematics are useful for simulations
      \item The diff-drive model used for moving roomba like robots
    \end{itemize}
  \end{frame}

  \begin{frame}\frametitle{References}
    \bibliographystyle{unsrt}
    \bibliography{MoR}
  \end{frame}

\end{document}
